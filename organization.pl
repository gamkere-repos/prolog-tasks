%Case 7

%Facts
organization(a).
organization(b).
organization(c).

profit(maximum).
profit(normal).

% Check unique
unique([]):-!.
unique([Head|Tail]):-
   member(Head, Tail), !, fail;
   unique(Tail).

% Generate all cases
solve(Solve):-
    Solve = [profit_organization(A, AProfit), profit_organization(B, BProfit), profit_organization(C, CProfit)],
    organization(A), organization(B), organization(C), unique([A,B,C]),
    profit(AProfit),profit(BProfit),profit(CProfit).

% Conditions
% If a == profit -> b == profit and c == profit
condition(1, Solve):-
    member(profit_organization(a, maximum), Solve),
    member(profit_organization(b, maximum), Solve),
    member(profit_organization(c, maximum), Solve);
    member(profit_organization(a, normal), Solve).

% If a == profit and c == profit or a != proft and c != profit
condition(2, Solve):-
    member(profit_organization(a, maximum), Solve),
    member(profit_organization(c, maximum), Solve);
    member(profit_organization(a, normal), Solve),
    member(profit_organization(c, normal), Solve).

% If c == pfofit -> b == profit
condition(3, Solve):-
    member(profit_organization(c, normal), Solve);
    member(profit_organization(c, maximum), Solve),
    member(profit_organization(b, maximum), Solve).

% Check all cases by conditions
check_hypothesis(Solve, TrueCondition):-
    solve(Solve), Solve = [profit_organization(a, _), profit_organization(b, _), profit_organization(c, _)],
    findall(Condition, condition(Condition, Solve), TrueCondition),
    TrueCondition = [_, _].
